package au.com.bglcorp

class TestUser {

    static constraints = {
    }

    String firstName
    String secondName
    String username
}
